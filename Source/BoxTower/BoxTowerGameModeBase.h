// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BoxTowerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BOXTOWER_API ABoxTowerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
