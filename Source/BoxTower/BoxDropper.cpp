// Fill out your copyright notice in the Description page of Project Settings.



#include "BoxDropper.h"

#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "BoxActor.h"



// Sets default values
ABoxDropper::ABoxDropper()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
	SpawnVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnVolume"));
	SpawnVolume->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	SpawnVolume->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SpawnVolume->BodyInstance.bLockXRotation = true;
	SpawnVolume->BodyInstance.bLockYRotation = true;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	StaticMeshComp->AttachToComponent(SpawnVolume, FAttachmentTransformRules::KeepRelativeTransform);
	StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>CubeMeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
	StaticMeshComp->SetStaticMesh(CubeMeshAsset.Object);
}

// Called when the game starts or when spawned
void ABoxDropper::BeginPlay()
{
	Super::BeginPlay();
	
	//setup input
	Player = Cast<ABoxPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	Player->OnDropBox.AddDynamic(this, &ABoxDropper::SpawnBoxActor);

	AActor* deathZone = UGameplayStatics::GetActorOfClass(GetWorld(), ADeathZone::StaticClass());

	if (IsValid(Cast<ADeathZone>(deathZone)))
	{
		DeathZone = Cast<ADeathZone>(deathZone);
		DeathZone->OnBoxDestroy.AddDynamic(this, &ABoxDropper::DestroyBoxActor);
	}
}

// Called every frame
void ABoxDropper::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
		
	FVector newLocation = GetActorLocation();
	float deltaHeight = (FMath::Sin(RunningTime + (DeltaTime*2)) - FMath::Sin(RunningTime));

	newLocation.X += deltaHeight * 200.0f;
	RunningTime += DeltaTime;
	SetActorLocation(newLocation);
}

void ABoxDropper::SpawnBoxActor()
{
	if (CanDropBox)
	{
		CanDropBox = false;
		StaticMeshComp->SetVisibility(false);
		
		FVector SpawnLocation = GetActorLocation();
		FRotator SpawnRotation = GetActorRotation();

		GetWorld()->SpawnActor<ABoxActor>(SpawnLocation, SpawnRotation);

		// Can increaSse height
		AdjustSpawnerHeight(100.f);
		
		OnIncreaseCameraHeight.Broadcast(100.f);

		//Increase box count
		NbBoxes(true);

		GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this,
													&ABoxDropper::ResetSpawning, 1.f, false);
	}	
}

void ABoxDropper::DestroyBoxActor()
{
	if (TowerHeight > 0)
	{
		AdjustSpawnerHeight(-100.f);
		NbBoxes(false);
		OnDecreaseCameraHeight.Broadcast(-100.f);
	}
}

//Changes spawner height when an overlap works
void ABoxDropper::AdjustSpawnerHeight(float heigthAdjustement)
{
	SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + heigthAdjustement),
					 false, nullptr, ETeleportType::None);
}

void ABoxDropper::ResetSpawning()
{
	StaticMeshComp->SetVisibility(true);
	CanDropBox = true;
}

void ABoxDropper::NbBoxes(bool stacked)
{
	(stacked) ? TowerHeight++ : TowerHeight--;
}

