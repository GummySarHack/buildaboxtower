// Fill out your copyright notice in the Description page of Project Settings.


#include "BoxPlayerController.h"

#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/GameplayStatics.h"
#include "BoxDropper.h"
#include "Plateform.h"

void ABoxPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("SpawnBox", IE_Pressed, this, &ABoxPlayerController::OnDropBoxAction);
	InputComponent->BindAction("PauseGame", IE_Pressed, this, &ABoxPlayerController::OnPauseGameAction);

	AActor* boxDropper = UGameplayStatics::GetActorOfClass(GetWorld(), ABoxDropper::StaticClass());

	if (IsValid(Cast<APlateform>(boxDropper)))
	{
		ABoxDropper* box = Cast<ABoxDropper>(boxDropper);
		
		SetViewTarget(box);
	}
}

void ABoxPlayerController::OnDropBoxAction()
{
	OnDropBox.Broadcast();
}

void ABoxPlayerController::OnPauseGameAction()
{
	OnPauseGame.Broadcast();
}