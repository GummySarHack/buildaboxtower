// Fill out your copyright notice in the Description page of Project Settings.


#include "CameraPosition.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/SpringArmComponent.h"


// Sets default values
ACameraPosition::ACameraPosition()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));

	//Attach our component
	CameraComp->SetupAttachment(SpringArmComp, USpringArmComponent::SocketName);

	//Assign SpringArm class variables.
	SpringArmComp->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 0.0f), FRotator(0.0f, 0.0f, -90.0f));
	SpringArmComp->TargetArmLength = 700.f;
	SpringArmComp->bEnableCameraLag = true;
	SpringArmComp->CameraLagSpeed = 3.0f;

	CameraComp->bUsePawnControlRotation = false;
}

// Called when the game starts or when spawned
void ACameraPosition::BeginPlay()
{
	Super::BeginPlay();

	AActor* boxDropper = UGameplayStatics::GetActorOfClass(GetWorld(), ABoxDropper::StaticClass());

	if (IsValid(Cast<ABoxDropper>(boxDropper)))
	{
		BoxDropper = Cast<ABoxDropper>(boxDropper);
		BoxDropper->OnIncreaseCameraHeight.AddDynamic(this, &ACameraPosition::AdjustSpawnerHeigth);
		BoxDropper->OnDecreaseCameraHeight.AddDynamic(this, &ACameraPosition::AdjustSpawnerHeigth);
	}
}

// Called every frame
void ACameraPosition::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//Changes spawner height when an overlap works
void ACameraPosition::AdjustSpawnerHeigth(float heigthAdjustement)
{
	SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + heigthAdjustement),
					 false, nullptr, ETeleportType::None);
}