// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BoxPlayerController.h"
#include "DeathZone.h"

#include "BoxDropper.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeHeight, float, height);


UCLASS()
class BOXTOWER_API ABoxDropper : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoxDropper();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBoxComponent* SpawnVolume;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* StaticMeshComp;

	UPROPERTY(EditAnywhere)
	float RunningTime;

	UPROPERTY()
	bool CanDropBox = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int TowerHeight = 0;

	FTimerHandle UnusedHandle;

	UFUNCTION()
	void ResetSpawning();

	UPROPERTY()
	ABoxPlayerController* Player;

	UPROPERTY()
	ADeathZone* DeathZone;

	float TimeToNextCameraChange;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SpawnBoxActor();

	UFUNCTION()
	void DestroyBoxActor();

	UFUNCTION()
	void AdjustSpawnerHeight(float heigthAdjustement);

	UFUNCTION()
	void NbBoxes(bool stacked);

	/** Delegate to whom anyone can subscribe to receive this event */
	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnChangeHeight OnIncreaseCameraHeight;

	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnChangeHeight OnDecreaseCameraHeight;


};
