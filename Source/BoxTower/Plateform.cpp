// Fill out your copyright notice in the Description page of Project Settings.


#include "Plateform.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
APlateform::APlateform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	
	static ConstructorHelpers::FObjectFinder<UStaticMesh>CubeMeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
	StaticMeshComp->SetStaticMesh(CubeMeshAsset.Object);

	StaticMeshComp->SetWorldScale3D(FVector(1.5f,1.5f,0.2f));
}

// Called when the game starts or when spawned
void APlateform::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlateform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
