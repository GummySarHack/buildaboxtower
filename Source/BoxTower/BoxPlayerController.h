// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BoxPlayerController.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDropBox);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPause);

UCLASS()
class BOXTOWER_API ABoxPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	/** Delegate to whom anyone can subscribe to receive this event */
	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnDropBox OnDropBox;

	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnPause OnPauseGame;

	virtual void SetupInputComponent() override;

	void OnDropBoxAction();

	UFUNCTION(BlueprintCallable)
	void OnPauseGameAction();
};
