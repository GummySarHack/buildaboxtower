// Fill out your copyright notice in the Description page of Project Settings.


#include "BoxActor.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Plateform.h"
#include "Components/PrimitiveComponent.h"

// Sets default values
ABoxActor::ABoxActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	StaticMeshComp->SetupAttachment(RootComponent);
	
	static ConstructorHelpers::FObjectFinder<UStaticMesh>CubeMeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Box.Shape_Box'"));
	StaticMeshComp->SetStaticMesh(CubeMeshAsset.Object);
	StaticMeshComp->SetSimulatePhysics(true);

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBox->AttachToComponent(StaticMeshComp, FAttachmentTransformRules::KeepRelativeTransform);
	CollisionBox->SetGenerateOverlapEvents(true);
	CollisionBox->SetCollisionProfileName(TEXT("BoxPreset"));
	CollisionBox->SetRelativeScale3D(FVector(2.f,2.f,2.f));
	CollisionBox->AddRelativeLocation(FVector(0.f, 0.f, 60.f));
	
	static ConstructorHelpers::FObjectFinder<USoundBase>SoundAsset(
		TEXT("SoundWave'/Game/StarterContent/Audio/BasketBallHit.BasketBallHit'"));
	Sound = SoundAsset.Object;
}

// Called when the game starts or when spawned
void ABoxActor::BeginPlay()
{
	Super::BeginPlay();

	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &ABoxActor::OnBoxBeginOverlap);

	if (StaticMeshComp)
	{
		float r = FMath::FRand();
		float g = FMath::FRand();
		float b = FMath::FRand();

		UMaterialInstanceDynamic* MIColour = UMaterialInstanceDynamic::Create(StaticMeshComp->GetMaterial(0), this);
		MIColour->SetVectorParameterValue(FName(TEXT("Colour")), FLinearColor(r, g, b, 1.0));
		StaticMeshComp->SetMaterial(0, MIColour);
	}
}

// Called every frame
void ABoxActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABoxActor::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
								  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
								  bool bFromSweep, const FHitResult& SweepResult)
{
	ABoxActor* boxActor = Cast<ABoxActor>(OtherActor);
	APlateform* plateform = Cast<APlateform>(OtherActor);

	if ((IsValid(boxActor) && (boxActor != this)) || IsValid(plateform))
	{
		/*if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Blue, TEXT("HIT!"));*/

		CollisionBox->OnComponentBeginOverlap.RemoveAll(this);

		//make sound
		UGameplayStatics::PlaySound2D(this, Sound, 1.f, 1.f, 0.f);
	}
}