// Fill out your copyright notice in the Description page of Project Settings.


#include "DeathZone.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"
#include "Sound/SoundBase.h"

// Sets default values
ADeathZone::ADeathZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Sphere"));
	BoxComp->SetWorldScale3D(FVector(20.f, 20.f,1.f));
	BoxComp->SetVisibility(false);
	BoxComp->SetHiddenInGame(false);

	static ConstructorHelpers::FObjectFinder<USoundBase>SoundAsset(
		TEXT("SoundWave'/Game/StarterContent/Audio/WaterDroplet.WaterDroplet'"));
	Sound = SoundAsset.Object;
}

// Called when the game starts or when spawned
void ADeathZone::BeginPlay()
{
	Super::BeginPlay();

	BoxComp->OnComponentBeginOverlap.AddDynamic(this, &ADeathZone::OnBoxBeginOverlap);
}

// Called every frame
void ADeathZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ADeathZone::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, 
								   UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto BoxActor = Cast<ABoxActor>(OtherActor);

	if (IsValid(BoxActor))
	{
		UGameplayStatics::PlaySound2D(this, Sound, 0.5f, 1.f, 0.f);
		BoxActor->Destroy();

		OnBoxDestroy.Broadcast();
	}
}
