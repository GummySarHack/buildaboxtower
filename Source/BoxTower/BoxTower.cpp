// Copyright Epic Games, Inc. All Rights Reserved.

#include "BoxTower.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BoxTower, "BoxTower" );
