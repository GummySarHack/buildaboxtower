# Project status
Done



## Test Unreal Engine C++ -- Build a tower.

From the Unreal Engine 5.1 Basic Templates. The aim is to create a high tower made of block.


## Features
* Box spawner that creates the boxes needed to make a tower
* The box spawner moves left and right
* Every box correctly dropped makes the spawner go higher
* If the box doesn't fall within a certain range, they fall over and get destroy
* If the box falls completly out of range, it also get destroy
* A camera follows the tower as you go higher - camera cannot go left or right --> dependant on the high of the tower
* Add main menu UI + UI to show nb boxes stashed
* Add material instance to box spawn. New colour for every box spawned
* Add sound when boxes collide and fall
* Add pause/exit menu 
* Added landscape
* Added packaged version